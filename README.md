# sealevel2100

This repository contains the source code of the web app for showcasing the results of
*Schuldt et al., 2020: Sea-Level Rise in Northern Germany: A GIS-Based Simulation and
Visualization*
([reference](https://link.springer.com/article/10.1007/s42489-020-00059-8)). The
intention is to enable an interactive exploration of possible consequences of
sealevel rise that are caused by climate change as a consequence of global warming.

The app is made with [Vue.js](https://vuejs.org/) v2 and uses the
[Leaflet](https://leafletjs.com/) library for the web map. Depending on how
the scenario datasets are supplied, it relies on [Geoserver](http://geoserver.org/) for
providing a WMS (Web Map Service).

For a primer on Vue.js consider the following resources:
- [Introduction](https://vuejs.org/v2/guide/)
- [Component basics](https://vuejs.org/v2/guide/components.html)
  - [Single file components (SFC)](https://vuejs.org/v2/guide/single-file-components.html)
- [Props](https://vuejs.org/v2/guide/components-props.html)
- [Slots](https://vuejs.org/v2/guide/components-slots.html)
- [Vue.js Explained in 100 Seconds](https://www.youtube.com/watch?v=nhBVL41-_Cw)

## Architecture

The web app is structured into several components that have a distinct task to solve.
With respect to a clear folder structure they are divided into
[Base](src/sealevel2100/components/Base), [Leaflet](src/sealevel2100/components/Leaflet)
and [Scenario](src/sealevel2100/components/Scenario) components.
### Main component

The main component ([Main.vue](src/sealevel2100/Main.vue)) is the entry point for the
whole app and combines all other components that make up the user interface (map, info
boxes). It also serves as configuration entry point for the map and kicks of processes
like loading the features to be displayed on the map from a server. It notifies all
other components that the scenarios have been loaded.

### Base components

The base components ([CheckBox.vue](src/sealevel2100/components/Base/CheckBox.vue),
[InfoBox.vue](src/sealevel2100/components/Base/InfoBox.vue),
[Loader.vue](src/sealevel2100/components/Base/Loader.vue)) are very basic and less
specialized components for the UI, which are meant to be reused often. Therefore they
have a simple and clean interface, that is easy to configure. The
[InfoOverlay](src/sealevel2100/components/Base/InfoOverlay.vue) component holds
information about the scientific background and is displayed above all other page
content when the user clicks the small info-icon in the
[ScenarioInfo](src/sealevel2100/components/Scenario/ScenarioInfo.vue) component.

### Leaflet components

The Leaflet functionality relies mainly on the [map
object](https://leafletjs.com/reference-1.7.1.html#map-example) to which other elements
like feature layers or UI elements like controls are added. For the use in this app
there are two Leaflet related components:

- [LMap.vue](src/sealevel2100/components/Leaflet/LMap.vue), which instantiates the map
  object, sets the used tile layer and provides properties for configuring the map
  appearance. It provides a slot that receives the map object as a [slot
  property](https://vuejs.org/v2/guide/components-slots.html#Scoped-Slots) for adding
  other UI elements to be displayed on top of the map.
- [LControl.vue](src/sealevel2100/components/Leaflet/LControl.vue), which is the base
  component for displaying a control element on the map. It needs a unique id and a
  reference to the map instance on which the control element should appear. A LControl
  component can then be used in the slot the LMap component provides.

#### Scenario components

These components provide the actual UI elements for displaying a specific scenario and
the data and information belonging to it:

- [ScenarioInfoBox.vue](src/sealevel2100/components/Scenario/ScenarioInfoBox.vue) is the
  base component holding instructions and markup about how to handle scenario related
  information. It emits an 'update' event when the scenario slider or the
  checkbox has been updated, so that it can be handled in the ScenarioInfo component.
- [ScenarioInfo.vue](src/sealevel2100/components/Scenario/ScenarioInfo.vue) holds
  scenario related information (affected people, flooded area, description of the
  scenario) and reacts to changes when the slider is moved.
- [ScenarioSlider.vue](src/sealevel2100/components/Scenario/ScenarioSlider.vue) is the
  main component controlling which scenario is displayed on the map and in the
  ScenarioInfo component.  It emits events `slider-changed` and `checkbox-updated`.

### Application state & communication between components

Vue provides no built-in application state management. However it provides an event
mechanism for communication between parent and child components. For handling
application wide events and state one has to use a solution like
[Vuex](https://vuex.vuejs.org/). For this small project a simple
[EventBus](https://andrejsabrickis.medium.com/https-medium-com-andrejsabrickis-create-simple-eventbus-to-communicate-between-vue-js-components-cdc11cd59860)
and a simple [Store](src/sealevel2100/Store.js) (basically a javascript module holding a
class which exported instance is accessed by multiple components) is sufficient, as the
complexity of the app is relatively low and is not to be thought to grow substantially.

## Configuration
### config.js

Basic configuration i.e. which technology is used to display the scenario datasets on
the map is controlled via the file [config.js](src/sealevel2100/config.js). There are
only two possibilities (`wms`, `topoJson`) of which `wms` is set to `true` by default
(only one of them can be `true` at a time otherwise the app will not work). Note that
when `topoJson` is set to true GeoJSON datasets will also work but TopoJSON is recommended
because of smaller file size.
### Scenario datasets (GeoJSON, TopoJSON or WMS)

Depending on the config settings made in the config.js module the url property of a
scenario json must point either to a WMS layer name (a string separated by a colon:
`sealevelrise:<layer-name>`) or a link or path to a Topo- or GeoJSON file on a server.

## Development

### Local development environment

For development the dependencies need to be installed first:

```bash
npm install
```

As mentioned above the app is made with Vue2. For bundling
[Webpack](https://webpack.js.org/) is used which also provides a development server for
local testing and rapid feedback for changes. The development server can be started like
so:

```bash
npm run start
```

A new window will open in the standard browser which runs the sealevel2100 app. When
changes are made to one of the apps files the page will automatically be reloaded.

### Using Geoserver

When using a WMS for providing the scenario datasets it is recommended to start a local
Geoserver instance. Luckily configuration is relatively easy as there is docker image
providing Geoserver. The easiest way is to used
[docker-compose](https://docs.docker.com/compose/install/). Once installed alongside
with [docker](https://docs.docker.com/get-docker/) the Geoserver instance can be started
like so:

```bash
docker-compose up -d
```

Be sure to set the `SEALEVEL2100_DATA` environment variable in the provided
[.env](./.env) file. It should point to the scenario files. If not set Geoserver will
not work correctly.

Geoserver provides a web interface for configuration which is accessible via
`localhost:8080/geoserver`. The initial user name is `admin` and the password is
`geoserver`.

For configuration refer to the Geoserver documentation. The first steps you should do is

1. Set up a workspace and name it `sealevelrise`
2. Add a data storage for the workspace `sealevelrise` (a folder containing shape files
   can be selected in the interface)
3. Add layers and make them accessible for the WMS. The layer names should be the same
   as the original shape files of the simulation.


## Deployment

### Building

For publishing the sealevel2100 app it needs to be built. This can be done like so:

```bash
npm run build
```

When the process is finished there should be a `dist` folder in the repository
containing the built artifacts . The contents of this folder need to be uploaded to the
webservers public directory.

### Web server configuration

For correct usage with the sealevel2100 app the WMS should be available via the `/wms`
url path.

### WMS performance

For best performance the built in
[GeoWebCache](https://docs.geoserver.org/stable/en/user/geowebcache/index.html) should
be used together with a WMS. Consider using the [MBTiles Blob
Store](https://docs.geoserver.org/stable/en/user/community/gwc-sqlite/index.html) for
caching tiles to overcome inode issues when there are a lot of cached tiles. This can be
enabled by setting the enviroment variable holding community extensions:

```bash
COMMUNITY_EXTENSIONS=gwc-sqlite-plugin
 ```
