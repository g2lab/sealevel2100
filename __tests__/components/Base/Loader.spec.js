import Vue from "vue";
import { shallowMount } from "@vue/test-utils";

import Loader from "../../../src/sealevel2100/components/Base/Loader.vue";

describe("Loader", () => {
    let vm;
    beforeEach(() => {
        vm = shallowMount(Loader);
    });
    it("shows a loader by default", () => {
        expect(vm.find(".sk-chase")).toBeTruthy();
    });
    it("has default width/height of 80px and color rgb(23, 112, 230)", () => {
        const spinner = vm.find(".sk-chase");
        expect(spinner.attributes().style).toContain("width: 80px;");
        expect(spinner.attributes().style).toContain("height: 80px;");
        expect(spinner.attributes().style).toContain(
            "--spinnerColor: rgb(23, 112, 230)",
        );
    });
    it("disables the loader when 'loading' is false", async () => {
        await vm.setProps({ loading: false });
        expect(vm.find(".sk-chase").exists()).toBeFalsy();
    });
    it("unsets the style for the loader overlay when 'loading' is false", async () => {
        const overlay = vm.find(".loader-overlay");
        expect(overlay.attributes().style).toBeUndefined();
        await vm.setProps({ loading: false });
        expect(overlay.attributes().style).toContain("z-index: unset;");
        // Querying background-color somehow doesn't work. Skip it for now.
        // expect(overlay.attributes().style).toEqual(
        //     expect.stringContaining("background-color: unset;"),
        // );
    });
});
