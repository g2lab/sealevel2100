/// <reference path="../src/sealevel2100/types.js" />
// @ts-check
import L from "leaflet";

import * as util from "../src/sealevel2100/util";
import config from "../src/sealevel2100/config";

/** @type {GeoJSON.FeatureCollection} */
const scenarioGeoJson = {
    type: "FeatureCollection",
    features: [
        {
            type: "Feature",
            geometry: {
                type: "Polygon",
                coordinates: [
                    [
                        [100.0, 0.0],
                        [101.0, 0.0],
                        [101.0, 1.0],
                        [100.0, 1.0],
                        [100.0, 0.0],
                    ],
                ],
            },
            properties: { a: 10 },
        },
        {
            type: "Feature",
            geometry: {
                type: "Polygon",
                coordinates: [
                    [
                        [30, 10],
                        [40, 40],
                        [20, 40],
                        [10, 20],
                        [30, 10],
                    ],
                ],
            },
            properties: { a: 50 },
        },
    ],
};

/** @type {CombinedScenario} */
const testScenarioNS = {
    dikes: {
        url: "/path/to/dikes-ns.geojson",
        features: null,
        info: {
            people_affected: 100,
            flooded_area: 400,
            part_affected: ["this", "that"],
        },
    },
    no_dikes: {
        url: "/path/to/no-dikes-ns.geojson",
        features: null,
        info: {
            people_affected: 200,
            flooded_area: 800,
            part_affected: ["this", "that"],
        },
    },
};

/** @type {CombinedScenarios} */
const combinedTestScenariosNS = {
    rcp45: { dikes: null, no_dikes: null },
    rcp85: testScenarioNS,
    rise14: { dikes: null, no_dikes: null },
};

/** @type {CombinedScenario} */
const testScenarioBS = {
    dikes: {
        url: "/path/to/dikes-bs.geojson",
        features: null,
        info: {
            people_affected: 10,
            flooded_area: 200,
            part_affected: ["this", "that"],
        },
    },
    no_dikes: {
        url: "/path/to/no-dikes-bs.geojson",
        features: null,
        info: {
            people_affected: 20,
            flooded_area: 300,
            part_affected: ["this", "that"],
        },
    },
};

/** @type {CombinedScenarios} */
const combinedTestScenariosBS = {
    rcp45: { dikes: null, no_dikes: null },
    rcp85: testScenarioBS,
    rise14: { dikes: null, no_dikes: null },
};

// Unfortunately jest does not support mocking specific functions from a single module
// in a straight forward way. Therefore instead of mocking the interface of
// loadScenarioJson we need to mock the lower level fetch-API it is using.
let response;
let fetch;
beforeEach(() => {
    response = {
        json: jest.fn().mockResolvedValue(scenarioGeoJson),
    };
    fetch = jest.fn().mockResolvedValue(response);
    global.fetch = fetch;
});
afterEach(() => {
    jest.resetAllMocks();
    jest.restoreAllMocks();
});

describe("scenarios", () => {
    describe("merge", () => {
        it("returns an empty object when no arguments are given", () => {
            expect(util.merge()).toEqual({});
        });
        it("returns the object if only one object is given", () => {
            const obj = { a: 1 };
            expect(util.merge(obj)).toEqual(obj);
        });
        it("merges equal properties of two objects into one", () => {
            const obj1 = {
                a: 10,
                b: 20,
            };
            const obj2 = {
                a: 100,
                b: 200,
            };
            expect(util.merge(obj1, obj2)).toEqual({
                a: 110,
                b: 220,
            });
        });
        it("concatenates properties that are arrays", () => {
            const obj1 = {
                a: 1,
                b: [1, 2],
            };
            const obj2 = {
                a: 2,
                b: [3, 4],
            };
            expect(util.merge(obj1, obj2)).toEqual({
                a: 3,
                b: [1, 2, 3, 4],
            });
        });
        it("merges properties that are objects", () => {
            const obj1 = {
                a: 1,
                b: { a: 1, b: 2, c: [1, 2] },
            };
            const obj2 = {
                a: 2,
                b: { a: 1, b: 2, c: [3, 4] },
            };
            expect(util.merge(obj1, obj2)).toEqual({
                a: 3,
                b: { a: 2, b: 4, c: [1, 2, 3, 4] },
            });
        });
        it("merges properties holding an array of objects without overwriting", () => {
            const obj1 = {
                a: 1,
                b: [{ a: 1, b: 2 }],
                c: { d: [{ x: 4, y: 5 }] },
            };
            const obj2 = {
                a: 2,
                b: [{ a: 2, b: 3 }],
                c: { d: [{ x: 6, y: 7 }] },
            };
            expect(util.merge(obj1, obj2)).toEqual({
                a: 3,
                b: [
                    { a: 1, b: 2 },
                    { a: 2, b: 3 },
                ],
                c: {
                    d: [
                        { x: 4, y: 5 },
                        { x: 6, y: 7 },
                    ],
                },
            });
        });
        it("merges properties holding an array of leaflet objects", () => {
            const obj1 = {
                dikes: { features: [L.point(0, 0)] },
                no_dikes: { features: [L.point(1, 1)] },
            };
            const obj2 = {
                dikes: { features: [L.point(2, 2)] },
                no_dikes: { features: [L.point(3, 3)] },
            };
            expect(util.merge(obj1, obj2)).toEqual({
                dikes: {
                    features: [
                        { x: 0, y: 0 },
                        { x: 2, y: 2 },
                    ],
                },
                no_dikes: {
                    features: [
                        { x: 1, y: 1 },
                        { x: 3, y: 3 },
                    ],
                },
            });
        });
    });
    describe("mergeInfos", () => {
        it("merges infos of baltic and north sea scenarios into one", () => {
            const mergedInfos = util.mergeInfos(testScenarioNS, testScenarioBS);
            expect(mergedInfos.dikes.info.flooded_area).toEqual(600);
            expect(mergedInfos.dikes.info.people_affected).toEqual(110);
            expect(mergedInfos.no_dikes.info.flooded_area).toEqual(1100);
            expect(mergedInfos.no_dikes.info.people_affected).toEqual(220);
        });
    });
    describe("mergeFeatures", () => {
        beforeEach(() => {
            util.forEachDikeScenario(scenario => {
                testScenarioNS[scenario].features = L.geoJSON(scenarioGeoJson);
                testScenarioBS[scenario].features = L.geoJSON(scenarioGeoJson);
            });
        });
        afterEach(() => {
            util.forEachDikeScenario(scenario => {
                testScenarioNS[scenario].features = null;
                testScenarioBS[scenario].features = null;
            });
        });
        it("merges features of baltic and north sea into an array of features", () => {
            const mergedFeatures = util.mergeFeatures(
                testScenarioNS,
                testScenarioBS,
            );
            expect(mergedFeatures.dikes.features).toHaveLength(2);
            expect(mergedFeatures.no_dikes.features).toHaveLength(2);
            expect(mergedFeatures.dikes.features[0]).toEqual(
                testScenarioNS.dikes.features,
            );
            expect(mergedFeatures.dikes.features[1]).toEqual(
                testScenarioBS.dikes.features,
            );
            expect(mergedFeatures.no_dikes.features[0]).toEqual(
                testScenarioNS.no_dikes.features,
            );
            expect(mergedFeatures.no_dikes.features[1]).toEqual(
                testScenarioBS.no_dikes.features,
            );
        });
    });
    describe("loadCombined", () => {
        const origTopoJson = L.topoJSON;
        const origConfig = { ...config };
        let stub;
        beforeEach(() => {
            config.topoJson = true;
            config.wms = false;
            stub = jest.fn();
            L.topoJSON = jest.fn().mockImplementation(() => stub);
        });
        afterEach(() => {
            Object.assign(config, origConfig);
            stub = null;
            L.topoJSON = origTopoJson;
        });
        it("loads a combined scenario consisting of dikes and no_dikes keys", async () => {
            const combined = await util.loadCombined(testScenarioNS);
            expect(combined).toEqual({
                dikes: {
                    ...testScenarioNS.dikes,
                    features: stub,
                },
                no_dikes: {
                    ...testScenarioNS.no_dikes,
                    features: stub,
                },
            });
            expect(L.topoJSON).toHaveBeenCalledTimes(2);
            expect(L.topoJSON).toHaveBeenCalledWith(scenarioGeoJson);
        });
    });
    describe("compileScenarios", () => {
        it("compiles scenarios for north sea and baltic sea into a combined scenario object", async () => {
            const scenarios = await util.compileScenarios(
                combinedTestScenariosNS,
                combinedTestScenariosBS,
            );
            expect(scenarios.rcp45.dikes.features.getLayers()).toHaveLength(0);
            expect(scenarios.rcp45.no_dikes.features.getLayers()).toHaveLength(
                0,
            );
            expect(scenarios.rcp85.dikes.features.getLayers()).toHaveLength(2);
            expect(scenarios.rcp85.no_dikes.features.getLayers()).toHaveLength(
                2,
            );
            expect(scenarios.rcp85.dikes.info.flooded_area).toEqual(600);
            expect(scenarios.rcp85.dikes.info.people_affected).toEqual(110);
            expect(scenarios.rcp85.no_dikes.info.flooded_area).toEqual(1100);
            expect(scenarios.rcp85.no_dikes.info.people_affected).toEqual(220);
        });
    });
});
