/// <reference path="./types.js" />
// @ts-check
import noUiSlider from "nouislider";
import L from "leaflet";
import * as topojson from "topojson-client";

import config from "./config";

/** @type {CombinedScenario} */
const emptyCombinedScenario = {
    // @ts-ignore
    dikes: { info: null },
    // @ts-ignore
    no_dikes: { info: null },
};

/** @type {CombinedScenarios} */
const emptyCombinedScenarios = {
    // @ts-ignore
    rcp45: { title: null, dikes: {}, no_dikes: {}, meta: null },
    // @ts-ignore
    rcp85: { title: null, dikes: {}, no_dikes: {}, meta: null },
    // @ts-ignore
    rise14: { title: null, dikes: {}, no_dikes: {}, meta: null },
};

// Inspired by
// - https://webkid.io/blog/maps-with-leaflet-and-topojson/
// - http://bl.ocks.org/hpfast/2fb8de57c356d8c45ce511189eec5d6a
L.TopoJSON = L.GeoJSON.extend({
    addData(data) {
        if (data.type === "Topology") {
            Object.keys(data.objects).forEach(key => {
                const geojson = topojson.feature(data, data.objects[key]);
                L.GeoJSON.prototype.addData.call(this, geojson);
            });
        } else {
            L.GeoJSON.prototype.addData.call(this, data);
        }
    },
});

L.topoJSON = (data, options) => new L.TopoJSON(data, options);

/**
 * Helper function for iterating over dike scenario keys
 *
 * @param {Function} cb The callback invoked for each scenario
 */
export function forEachDikeScenario(cb) {
    if (!cb || typeof cb !== "function") return;
    ["dikes", "no_dikes"].forEach(dikeScenario => {
        cb(dikeScenario);
    });
}

/**
 * Helper function for iterating over scenario keys
 *
 * @param {Function} cb The callback invoked for each scenario
 */
export function forEachScenario(cb) {
    if (!cb || typeof cb !== "function") return;
    ["rcp45", "rcp85", "rise14"].forEach(scenario => {
        cb(scenario);
    });
}

/**
 * Merges properties of multiple objects into one object
 *
 * Handles properties that are numbers, arrays and objects.
 *
 * @param {...Object} objects The objects to merge
 * @returns {Object} An object with merged properties of the input objects
 */
export function merge(...objects) {
    return [...objects].reduce((acc, cur) => {
        Object.entries(cur).forEach(([key, val]) => {
            if (Array.isArray(val)) {
                acc[key] = (acc[key] ?? []).concat(val);
            } else if (typeof val === "object" && val !== null) {
                acc[key] = merge(acc[key] ?? {}, val);
            } else if (val === null) {
                acc[key] = val;
            } else {
                acc[key] = (acc[key] ?? 0) + val;
            }
        });
        return acc;
    }, {});
}

/**
 * Load the GeoJSON dataset of a scenario
 *
 * @param {Scenario} scenario
 * @returns {Promise<GeoJSON.FeatureCollection>} The loaded GeoJSON
 */
export async function loadScenarioDataset(scenario) {
    const response = await fetch(scenario.url);
    return response.json();
}

/**
 * Merge the info of the combined northsea features with those of the baltic sea
 *
 * Combines the north sea and baltic sea scenario info by adding up the respective
 * properties. Returns an empty object if a scenarios info is undefined.
 *
 * @param {CombinedScenario} north Combined scenario for the north sea
 * @param {CombinedScenario} baltic Combined scenario for the baltic sea
 * @returns {CombinedScenario} A combined scenario with the merged infos
 */
export function mergeInfos(north, baltic) {
    const merged = { ...emptyCombinedScenario };
    forEachDikeScenario(scenario => {
        merged[scenario] = merge(
            {
                info:
                    typeof north[scenario]?.info !== "undefined"
                        ? north[scenario].info
                        : {},
            },
            {
                info:
                    typeof baltic[scenario]?.info !== "undefined"
                        ? baltic[scenario].info
                        : {},
            },
        );
    });
    return merged;
}

/**
 * Merge the features of the combined north sea features with those of the baltic sea
 *
 * Combines the north sea and baltic sea features into an array of features. Returns an
 * empty list when the feature property of a scenario is undefined.
 *
 * @param {CombinedScenario} north Combined scenario for the north sea
 * @param {CombinedScenario} baltic Combined scenario for the baltic sea
 * @returns {CombinedScenario} A combined scenario with the merged features
 */
export function mergeFeatures(north, baltic) {
    const merged = { ...emptyCombinedScenario };
    forEachDikeScenario(scenario => {
        merged[scenario] = merge(
            {
                features:
                    typeof north[scenario]?.features !== "undefined"
                        ? [north[scenario].features]
                        : [],
            },
            {
                features:
                    typeof baltic[scenario]?.features !== "undefined"
                        ? [baltic[scenario].features]
                        : [],
            },
        );
    });
    return merged;
}

/**
 * Load the features of a scenario
 *
 * Loads the scenario dataset, which is either a WMS, a TopoJSON or a GeoJSON. When
 * neither wms nor topoJson parameter are set to true the features of the dataset will
 * be stored as a L.GeoJSON object.
 *
 * The scenario url should point to the respective WMS url, resp.  the url at which the
 * TopoJSON or GeoJSON is stored. The resulting Leaflet object is assigned to the
 * features property of the scenario object.
 *
 * @param {Scenario} scenario A single scenario
 * @param {boolean} wms Whether the scenario points to a WMS url
 * @param {boolean} topoJson Whether the scenario points to a TopoJSON file
 * @throws Throws an error when wms and topoJson are both true at the same time
 * @returns {Promise<Scenario>} The scenario with loaded features
 */
export async function loadFeatures(
    scenario,
    wms = config.wms,
    topoJson = config.topoJson,
) {
    const loadedScenario = { ...scenario };
    if (wms && topoJson) {
        throw Error(
            "Features cannot be supplied as WMS and TopoJSON at the same time.",
        );
    }
    let features = null;
    if (wms) {
        features = L.tileLayer.wms("/wms?", {
            layers: scenario.url,
            format: "image/png",
            transparent: true,
        });
    } else {
        const json = await loadScenarioDataset(scenario);
        features = topoJson ? L.topoJSON(json) : L.geoJSON(json);
    }
    loadedScenario.features = features;
    return loadedScenario;
}

/**
 * Load the combined features of a scenario
 *
 * Loads the features for the dike/no_dike scenarios
 *
 * @param {CombinedScenario} scenario A combined scenario
 * @returns {Promise<CombinedScenario>} The combined scenario with loaded features
 */
export async function loadCombined(scenario) {
    const combined = { ...scenario };
    combined.dikes =
        scenario.dikes !== null ? await loadFeatures(scenario.dikes) : null;
    combined.no_dikes =
        scenario.no_dikes !== null
            ? await loadFeatures(scenario.no_dikes)
            : null;
    return combined;
}

/**
 * Load all scenarios (rcp45, rcp85, rise14) for a specific sea
 *
 * @param {CombinedScenarios} sea The sea scenarios
 * @returns {Promise<CombinedScenarios>} The combined scenarios with loaded features
 */
export async function loadAllScenarios(sea) {
    const scenarios = [];
    forEachScenario(scenario => {
        scenarios.push(loadCombined(sea[scenario]));
    });
    const loaded = await Promise.all(scenarios);
    const combined = { ...emptyCombinedScenarios };
    Object.keys(sea).forEach((scenario, i) => {
        combined[scenario] = loaded[i];
    });
    return combined;
}

/**
 * Compile the combined scenarios for north sea and baltic sea
 *
 * Loads the respective scenarios for north an baltic sea, merges the infos for each
 * dike scenario and sea as well as the features for each dike scenario and sea. The
 * features per dike scenario are combined in a L.LayerGroup.
 *
 * @param {CombinedScenarios} north The unloaded combined north sea scenarios
 * @param {CombinedScenarios} baltic The unloaded combined baltic sea scenarios
 * @returns {Promise<MergedScenarios>} The merged scenarios
 */
export async function compileScenarios(north, baltic) {
    const NS = await loadAllScenarios(north);
    const BS = await loadAllScenarios(baltic);
    const merged = { ...emptyCombinedScenarios };
    const mergedInfos = { ...emptyCombinedScenarios };
    const mergedFeatures = { ...emptyCombinedScenarios };
    forEachScenario(scenario => {
        mergedInfos[scenario] = mergeInfos(NS[scenario], BS[scenario]);
        mergedFeatures[scenario] = mergeFeatures(NS[scenario], BS[scenario]);
        merged[scenario] = merge(
            mergedInfos[scenario],
            mergedFeatures[scenario],
        );
        merged[scenario].title = scenario.toUpperCase();
        forEachDikeScenario(dikeScenario => {
            merged[scenario][dikeScenario].features = L.layerGroup(
                merged[scenario][dikeScenario].features,
            );
        });
    });
    // @ts-ignore
    return merged;
}

/**
 * Convert an object holding information to an array of objects with title and content
 * keys
 *
 * To be used in ScenarioInfo and ScenarioMeta components
 *
 * @param {Object} obj An object holding information
 * @param {("info"|"meta")} kind The kind of info
 * @returns {Object[]} The converted objects
 */
export function convertToItems(obj, kind) {
    const info = obj[kind];
    const items = [];
    Object.keys(info).forEach(key => {
        const item = {};
        item.title = key;
        item.content = info[key];
        items.push(item);
    });
    return items;
}

/**
 * Add the features for all scenarios to a Leaflet map
 *
 * @param {MergedScenarios} scenarios The scenarios
 * @param {L.Map} map The Leaflet map instance
 */
export function addScenariosToMap(scenarios, map) {
    forEachScenario(scenario => {
        forEachDikeScenario(dikeScenario => {
            scenarios[scenario][dikeScenario].features.addTo(map);
            scenarios[scenario][dikeScenario].features.hide();
        });
    });
}

/**
 * Create a noUiSlider instance for switching between the scenarios
 *
 * @param {HTMLDivElement} el The element on which the slider is mounted
 * @param {MergedScenarios} scenarios The scenarios
 * @returns {Object} The noUiSlider API
 */
export function createSlider(el, scenarios) {
    const scenariosKeys = Object.keys(scenarios);
    return noUiSlider.create(el, {
        start: [0],
        format: {
            from(value) {
                return parseInt(value, 10);
            },
            to(value) {
                return scenariosKeys[value];
            },
        },
        step: 1,
        range: {
            min: 0,
            max: scenariosKeys.length - 1,
        },
        pips: {
            mode: "positions",
            stepped: true,
            values: scenariosKeys.map(
                (_, index, arr) => (index / (arr.length - 1)) * 100,
            ),
            density: 100 / (scenariosKeys.length - 1),
            format: {
                to(value) {
                    return `Szenario ${value + 1}`;
                },
            },
        },
    });
}

/**
 * Set display style for the scenario features.
 *
 * The given style object must adhere to the Leaflet Path options object properties
 * (https://leafletjs.com/reference-1.7.1.html#path-option)
 * @param {MergedScenario} scenarios The scenarios
 * @param {L.PathOptions} style An object containing style options
 */
export function setScenariosStyle(scenarios, style) {
    forEachScenario(scenario => {
        forEachDikeScenario(dikeScenario =>
            scenarios[scenario][dikeScenario].features.eachLayer(layer =>
                layer.setStyle(style),
            ),
        );
    });
}

/**
 * Add textual descriptions for the single scenarios to the merged scenarios.
 *
 * scenarios and descriptions need to have the same keys.
 *
 * @param {MergedScenarios} scenarios The scenarios
 * @param {object} descriptions The description for a scenario
 */
export function addScenarioDescriptions(scenarios, descriptions) {
    forEachScenario(scenario => {
        scenarios[scenario].meta = descriptions[scenario];
    });
}
