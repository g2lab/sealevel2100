const path = require("path");
const fetch = require("node-fetch");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const wmsRequest = async (urlPath, origReq, origRes) => {
    // eslint-disable-next-line no-unused-vars
    const [_, query] = origReq.originalUrl.split("?");
    fetch(`http://localhost:8600${urlPath}?${query}`)
        .then(response => response.buffer())
        .then(data => origRes.send(data))
        .catch(err => console.error(err));
};

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "sealevel2100.js",
        path: path.resolve(__dirname, "dist"),
    },
    devtool: "source-map",
    devServer: {
        index: "./dist/index.html",
        contentBase: "./dist/",
        writeToDisk: true,
        before(app) {
            app.get("/geoserver", (req, res) => {
                res.redirect("http://localhost:8600/geoserver");
            });
            app.get("/wms", (req, res) => {
                wmsRequest("/geoserver/gwc/service/wms", req, res);
            });
            app.get("/wms_uncached", (req, res) => {
                wmsRequest("/geoserver/wms", req, res);
            });
        },
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"],
            },
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8000,
                            name: "images/[hash]-[name].[ext]",
                        },
                    },
                ],
            },
            { test: /\.vue$/, loader: "vue-loader" },
            {
                test: /\.scss$/,
                // use: ["vue-style-loader", "css-loader", "sass-loader"],
                use: [
                    "vue-style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                        },
                    },
                    "sass-loader",
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "sealevel2100.css",
        }),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: "src/index.html",
        }),
        new CopyPlugin({
            patterns: [{ from: "src/sealevel2100/assets", to: "assets" }],
        }),
    ],
};
